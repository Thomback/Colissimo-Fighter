// Copyright Epic Games, Inc. All Rights Reserved.

#include "ColissimoFighter.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, ColissimoFighter, "ColissimoFighter" );
